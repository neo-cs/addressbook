# Contributing to Address book

:+1: :tada: First off, thanks for taking the time to contribute! :tada: :+1:

The following is a set of guidelines for contributing to Address book,
which are hosted in the [Address book](https://github.com/neo-cs/addressbook) on
GitHub. These are mostly guidelines, not rules. Use your best judgment, and feel
 free to propose changes to this document in a pull request.


#### Table Of Contents

[Code of Conduct](#code-of-conduct)

[What should I know before I get started?](#what-should-i-know-before-i-get-started)
  * [Design Decisions](#design-decisions)

[How Can I Contribute?](#how-can-i-contribute)
  * [Reporting Bugs](#reporting-bugs)
  * [Suggesting Enhancements](#suggesting-enhancements)
  * [Your First Code Contribution](#your-first-code-contribution)
  * [Pull Requests](#pull-requests)

[Styleguides](#styleguides)
  * [Git Commit Messages](#git-commit-messages)
  * [Graphic User Interface](#gui-styleguide)
  * [Java Styleguide](#java-styleguide)
  * [JavaScript Styleguide](#javascript-styleguide)
  * [Documentation Styleguide](#documentation-styleguide)

[Additional Notes](#additional-notes)
  * [Issue and Pull Request Labels](#issue-and-pull-request-labels)

## Code of Conduct
This project and everyone participating in it is governed by the 
[Address book Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are 
expected to uphold this code. Please report unacceptable behavior to 
[freddy.barrera.moo@gmail.com](mailto:freddy.barrera.moo@gmail.com).

## What should I know before I get started?
### Design Decisions
The maximum goal for the user interface is keep it simple and accessible for
everyone for this reason you can use the [Google Material Design Guidelines](https://material.io/guidelines/material-design/introduction.html)
as basis guide for the user interface.

## How Can I Contribute?
### Reporting Bugs

This section guides you through submitting a bug report for Address book. 
Following these guidelines helps maintainers and the community understand your
report :pencil:, reproduce the behavior :computer:, and find related reports :mag_right:.

When you are creating a bug report, please [include as many details as possible](#how-do-i-submit-a-good-bug-report).
Fill out [the required template](ISSUE_TEMPLATE.md), the information it asks for
helps us resolve issues faster.

> **Note:** If you find a **Closed** issue that seems like it is the same thing
that you're experiencing, open a new issue and include a link to the original
issue in the body of your new one.

#### How Do I Submit A (Good) Bug Report?

Bugs are tracked as [GitHub issues](https://guides.github.com/features/issues/).
Create an issue following information by filling in [the template](ISSUE_TEMPLATE.md).

Explain the problem and include additional details to help maintainers reproduce
 the problem:

* **Use a clear and descriptive title** for the issue to identify the problem.
* **Describe the exact steps which reproduce the problem** in as many details as
 possible. When listing steps, **don't just say what you did, but explain how 
you did it**.
* **Provide specific examples to demonstrate the steps**. Include links to files
 or GitHub projects, or copy/pasteable snippets, which you use in those examples.
If you're providing snippets in the issue, use [Markdown code blocks](https://help.github.com/articles/markdown-basics/#multiple-lines).
* **Describe the behavior you observed after following the steps** and point out
 what exactly is the problem with that behavior.
* **Explain which behavior you expected to see instead and why.**
* **Include screenshots and animated GIFs** which show you following the 
described steps and clearly demonstrate the problem. If you use the keyboard
while following the steps, you can use [this tool](https://www.cockos.com/licecap/)
 to record GIFs on macOS and Windows, and [this tool](https://github.com/colinkeenan/silentcast)
 or [this tool](https://github.com/GNOME/byzanz) on Linux.
* **If the problem is related to performance or memory**, include a 
[CPU profile capture](https://developers.google.com/web/tools/chrome-devtools/evaluate-performance/reference) 
with your report.

Provide more context by answering these questions:

* **Did the problem start happening recently** (e.g. after updating to a new 
version of Address book) or was this always a problem?
* If the problem started happening recently, **can you reproduce the problem in
 an older version of Address book?** What's the most recent version in which the
 problem doesn't happen? You can download older versions of Address book from
 [the releases page](https://github.com/neo-cs/addressbook/releases).

Include details about your configuration and environment:

* **What's the name and version of the OS you're using**?

### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for 
Address book, including completely new features and minor improvements to 
existing functionality. Following these guidelines helps maintainers and the
 community understand your suggestion :pencil: and find related suggestions :mag_right:.

Before creating enhancement suggestions, please check
 [this list](#before-submitting-an-enhancement-suggestion) as you might find out
 that you don't need to create one. When you are creating an enhancement
 suggestion, please [include as many details as possible](#how-do-i-submit-a-good-enhancement-suggestion).
 Fill in [the template](ISSUE_TEMPLATE.md), including the steps that you imagine
 you would take if the feature you're requesting existed.

#### Before Submitting An Enhancement Suggestion

* **Check if you're using the latest version of Address book.**
* **Perform a [cursory search](https://github.com/search?q=+is%3Aissue+user%3Aneo-cs+repo%3Aaddressbook)**
 to see if the enhancement has already been suggested. If it has, add a comment
 to the existing issue instead of opening a new one.

#### How Do I Submit A (Good) Enhancement Suggestion?

Enhancement suggestions are tracked as [GitHub issues](https://guides.github.com/features/issues/). After you've determined [which repository](#atom-and-packages) your enhancement suggestion is related to, create an issue on that repository and provide the following information:

<!--
(TODO: https://github.com/atom/atom/blob/master/CONTRIBUTING.md)
(TODO: https://github.com/puppetlabs/puppet/blob/master/CONTRIBUTING.md)
-->
