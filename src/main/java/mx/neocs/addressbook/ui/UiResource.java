/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.ui;

import mx.neocs.addressbook.config.ApplicationConstants;

import org.jboss.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service for the theme.
 * <table summary="Details for REST Web Service for the theme.">
 *   <thead>
 *     <tr>
 *       <th>Method</th>
 *       <th>URL</th>
 *       <th>Action</th>
 *     </tr>
 *   </thead>
 *   <tbody>
 *     <tr>
 *       <td><code>GET</code></td>
 *       <td>/rs/ui/</td>
 *       <td>Returns the theme available.</td>
 *     </tr>
 *   </tbody>
 * </table>
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
@Path("ui")
@RequestScoped
public class UiResource {

    @Context private UriInfo context;
    private static final Logger LOGGER = Logger.getLogger(UiResource.class.getName());

    /**
     * Allows to get the name of the theme.
     * @return the name of the theme.
     */
    @GET
    @Path("theme")
    @Produces(value = MediaType.TEXT_PLAIN)
    public String getTheme() {
        String applicationMode = ApplicationConstants.getApplicationMode();

        if (applicationMode == null) {
            LOGGER.warnv("The system property \"{0}\" is no set on {1}.", 
                    ApplicationConstants.APP_MODE, "standalone.xml");
            return "skin-blue-light";
        }
        
        switch (applicationMode) {
            case "debug" :
                return "skin-blue";
            case "production" :
                return "skin-black-light";
            default :
                LOGGER.warnv("The value \"{0}\" set on system property \"{1}\" is no valid.",
                        applicationMode, ApplicationConstants.APP_MODE);
                return "skin-blue-light";
        }
    }
}
