/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.ui;

import org.jboss.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
@Path("ui/modules/{moduleName: (((\\%\\w{2}|\\d{2})*([a-zA-Z\\-\\+]+))+)}")
@RequestScoped
public class ModuleResource {

    private static final Logger LOG = Logger.getLogger(ModuleResource.class.getName());

    @Context private UriInfo context;
    @PathParam("moduleName") private String moduleName;
    
    /**
     * Retrieves representation of an instance of mx.neocs.addressbook.ui.ModuleResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        LOG.debugv("Modulo prueba {0}", moduleName);

        return "Modulo prueba " +  moduleName;
    }

}
