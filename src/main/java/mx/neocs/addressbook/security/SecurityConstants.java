/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.security;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class SecurityConstants {

    /** Defines the name of the attribute that represents the user and who is in the session. */
    public static final String SESSION_ATRIBUTE_LOGGED_USER = "ADDRESS-BOOK__USR";

    /** Defines the name of the cookie that has the user's session. */
    public static final String COOKIE_NAME_FOR_TOKEN = "ADDRESS-BOOK__TKN";

    /** Defines the URL of the project home page without including the context path. */
    public static final String HOME_PAGE_URL = "index.xhtml";

    /** Defines the URL of the login page of the project without including the context path. */
    public static final String LOGIN_PAGE_URL = "security/login.xhtml";

    /** Defines the URL of the unauthorized page (error 401). */
    public static final String UNAUTHORIZED_PAGE_URL = "security/unauthorized.xhtml";

    /** Defines the URL of the project logout page without including the context path. */
    public static final String LOGOUT_PAGE_URL = "security/logout.xhtml";

    /** Defines the duration of the token in seconds. */
    public static final int SESSION_TIMEOUT = 60 * 60; // One hour.

    /** Max limit login attempts before block user. */
    public static int LOGIN_ATTEMPTS = 3;

    private SecurityConstants() {
    }

}
