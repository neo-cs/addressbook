/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.persistence;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class ContactRepository extends GenericRepository<ContactEntity, Long> {
    
    private static final long serialVersionUID = -3787070710038963036L;
    
    /**
     * Allows to add a new phone to a contact.
     * @param phoneEntity the new phone for the contact.
     */
    public void add(PhoneEntity phoneEntity) {
        entityManager.persist(phoneEntity);
    }

    /**
     * Allow to add a new email to the contact.
     * @param emailEntity the new email for the contact.
     */
    public void add(EmailEntity emailEntity) {
        entityManager.persist(emailEntity);
    }

    /**
     * Allows to find a phone using the contact identifier and the phone identifier.
     * @param contactId the contact identifier.
     * @param phoneId the phone identifier.
     * @return if and only if is found the phone entity.
     */
    public PhoneEntity findPhone(Long contactId, Long phoneId) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<PhoneEntity> cq = cb.createQuery(PhoneEntity.class);
        Root<PhoneEntity> phone = cq.from(PhoneEntity.class);
        Join<PhoneEntity, ContactEntity> join = phone.join(PhoneEntity_.contact);
        cq.where(cb.and(cb.equal(join.get(ContactEntity_.id), contactId),
                cb.equal(phone.get(PhoneEntity_.id), phoneId)));

        return entityManager.createQuery(cq).getSingleResult();
    }

    /**
     * Allows to get whole the contact phones numbers.
     * @param contactId the contact identifier.
     * @return a list with all the phones from the contact.
     */
    public List<PhoneEntity> getPhones(Long contactId) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<PhoneEntity> cq = cb.createQuery(PhoneEntity.class);
        Root<PhoneEntity> phone = cq.from(PhoneEntity.class);
        Join<PhoneEntity, ContactEntity> join = phone.join(PhoneEntity_.contact);
        cq.where(cb.equal(join.get(ContactEntity_.id), contactId));

        return entityManager.createQuery(cq).getResultList();
    }

    /**
     * Allows to get all the information of a specific contact email address.
     * @param contactId the contact identifier.
     * @param emailId the email identifier.
     * @return if and only if is found the email entity.
     */
    public EmailEntity findEmail(Long contactId, Long emailId) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<EmailEntity> cq = cb.createQuery(EmailEntity.class);
        Root<EmailEntity> email = cq.from(EmailEntity.class);
        Join<EmailEntity, ContactEntity> join = email.join(EmailEntity_.contact);
        cq.where(cb.and(cb.equal(join.get(ContactEntity_.id), contactId),
                cb.equal(email.get(EmailEntity_.id), emailId)));

        return entityManager.createQuery(cq).getSingleResult();
    }

    /**
     * Allows to get whole the contact email addresses.
     * @param contactId the contact identifier.
     * @return a list with all the email addresses from the contact.
     */
    public List<EmailEntity> getEmails(Long contactId) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<EmailEntity> cq = cb.createQuery(EmailEntity.class);
        Root<EmailEntity> email = cq.from(EmailEntity.class);
        Join<EmailEntity, ContactEntity> join = email.join(EmailEntity_.contact);
        cq.where(cb.equal(join.get(ContactEntity_.id), contactId));

        return entityManager.createQuery(cq).getResultList();
    }

    /**
     * Allows update the phone information.
     * @param phoneEntity the phone information that will be update.
     */
    public void update(PhoneEntity phoneEntity) {
        entityManager.merge(phoneEntity);
    }

    /**
     * Allows update the email information.
     * @param emailEntity the email information that will be update.
     */
    public void update(EmailEntity emailEntity) {
        entityManager.merge(emailEntity);
    }

    /**
     * Allows to remove a phone.
     * @param phoneEntity the phone that will be remove.
     */
    public void deletePhone(PhoneEntity phoneEntity) {
        entityManager.remove(phoneEntity);        
    }

    /**
     * Allows to remove a email.
     * @param emailEntity the email that will be remove.
     */
    public void deleteEmail(EmailEntity emailEntity) {
        entityManager.remove(emailEntity);
    }

}
