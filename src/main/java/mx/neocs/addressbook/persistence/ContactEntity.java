/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.persistence;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
@Entity
@Table(name = "contacts")
public class ContactEntity implements Serializable {

    private static final long serialVersionUID = 4305630942751781284L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "contact_id")
    private Long id;

    @Column(name = "title")
    private String title;
    
    @Column(name = "first_name")
    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "^[^\\s]+$", message = "Not allowed white spaces")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;
    
    @Column(name = "last_name")
    private String lastName;
    
    @Column(name = "suffix")
    private String suffix;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get the value of title.
     *
     * @return the value of title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the value of title.
     *
     * @param title new value of title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get the value of firstName.
     *
     * @return the value of firstName.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the value of firstName.
     *
     * @param firstName new value of firstName.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Get the value of middleName.
     *
     * @return the value of middleName.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Set the value of middleName.
     *
     * @param middleName new value of middleName.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    

    /**
     * Get the value of lastName.
     *
     * @return the value of lastName.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the value of lastName.
     *
     * @param lastName new value of lastName.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get the value of suffix.
     *
     * @return the value of suffix.
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Set the value of suffix.
     *
     * @param suffix new value of suffix.
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.id);
        hash = 71 * hash + Objects.hashCode(this.title);
        hash = 71 * hash + Objects.hashCode(this.firstName);
        hash = 71 * hash + Objects.hashCode(this.middleName);
        hash = 71 * hash + Objects.hashCode(this.lastName);
        hash = 71 * hash + Objects.hashCode(this.suffix);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof ContactEntity)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        final ContactEntity other = (ContactEntity) object;
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.middleName, other.middleName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.suffix, other.suffix)) {
            return false;
        }
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "ContactEntity{" 
                + "id : " + id 
                + ", title : " + title 
                + ", firstName : " + firstName 
                + ", middleName : " + middleName 
                + ", lastName : " + lastName 
                + ", suffix : " + suffix
                + '}';
    }
    
}
