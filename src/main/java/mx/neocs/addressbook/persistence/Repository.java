/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.persistence;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @param <T> the entity type.
 * @param <K> the key type.
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public interface Repository<T extends Serializable, K extends Serializable> extends Serializable {

    /**
     * This method save a new person in the datasource.
     * @param entity the new person.
     * @return the entity created.
     */
    T create(T entity);

    /**
     * This method get the information about a person.
     * @param key the ID of the person to look up.
     * @return a person.
     */
    T read(K key);

    /**
     * This method get row count of the entities.
     * @return row count of the entities.
     */
    Long getEntityCount();

    /**
     * This method get all entities in the datasource.
     * @return a list with all entities in the datasource.
     */
    List<T> findEntities();

    /**
     * This method get all entities in the datasource but paged.
     * @param maxResults the page size.
     * @param firstResult the position of the first result.
     * @return a list with entities in the datasource.
     */
    List<T> findEntities(int maxResults, int firstResult);
    
    /**
     * This method update the information of a person.
     * @param entity the information to update.
     */
    void update(T entity);

    /**
     * This method delete the information of a person.
     * @param key the ID form the person will be delete.
     */
    void delete(K key);
}
