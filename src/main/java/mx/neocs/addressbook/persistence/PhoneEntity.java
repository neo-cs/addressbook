/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.persistence;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
@Entity
@Table(name = "phones")
public class PhoneEntity implements Serializable {

    private static final long serialVersionUID = -9169996007557170240L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "phone_id")
    private Long id;
    
    @Column(name = "phone")
    @Pattern(regexp = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
            message = "{invalid.phonenumber}")
    private String phone;
    
    @Column(name = "phone_type")
    @Enumerated(EnumType.STRING)
    @NotNull
    private PhoneType phoneType;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_id", nullable = false)
    private ContactEntity contact;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Get the value of phone.
     *
     * @return the value of phone.
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Set the value of phone.
     *
     * @param phone new value of phone.
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }


    /**
     * Get the value of phoneType.
     *
     * @return the value of phoneType.
     */
    public PhoneType getPhoneType() {
        return phoneType;
    }

    /**
     * Set the value of phoneType.
     *
     * @param phoneType new value of phoneType.
     */
    public void setPhoneType(PhoneType phoneType) {
        this.phoneType = phoneType;
    }


    /**
     * Get the value of contact.
     *
     * @return the value of contact.
     */
    public ContactEntity getContact() {
        return contact;
    }

    /**
     * Set the value of contact.
     *
     * @param contact new value of contact.
     */
    public void setContact(ContactEntity contact) {
        this.contact = contact;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.id);
        hash = 41 * hash + Objects.hashCode(this.phone);
        hash = 41 * hash + Objects.hashCode(this.phoneType);
        hash = 41 * hash + Objects.hashCode(this.contact);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PhoneEntity other = (PhoneEntity) obj;
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (this.phoneType != other.phoneType) {
            return false;
        }

        return Objects.equals(this.contact, other.contact);
    }

    @Override
    public String toString() {
        return "PhoneEntity{" 
                + "id : " + id 
                + ", phone : " + phone 
                + ", phoneType : " + phoneType 
                + ", contact : " + contact 
                + '}';
    }

}
