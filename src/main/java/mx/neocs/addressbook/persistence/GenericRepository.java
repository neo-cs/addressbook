/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.persistence;

import mx.neocs.addressbook.config.ApplicationConstants;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @param <T> the entity type.
 * @param <K> the key type.
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class GenericRepository<T extends Serializable, K extends Serializable>
        implements Repository<T, K> {

    private static final long serialVersionUID = 6621929794450653016L;

    @PersistenceContext(unitName = ApplicationConstants.PERSISTENCE_UNIT)
    protected EntityManager entityManager;
    private Class<T> entityClass;

    private Class<?> getParameterClass(Class<?> clazz, int index) {
        return (Class<?>) (((ParameterizedType) clazz.getGenericSuperclass())
                .getActualTypeArguments()[index]);
    }

    protected synchronized Class<T> getEntityClass() {
        if (entityClass == null) {
            entityClass = (Class<T>) getParameterClass(getClass(), 0);
        }
        return entityClass;
    }

    @Override
    public T create(T entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public Long getEntityCount() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        Root<T> rt = cq.from(getEntityClass());
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        Query query = entityManager.createQuery(cq);
        return ((Long) query.getSingleResult());
    }

    @Override
    public List<T> findEntities() {
        return findEntities(true, -1, -1);
    }

    @Override
    public List<T> findEntities(int maxResults, int firstResult) {
        return findEntities(false, maxResults, firstResult);
    }

    private List<T> findEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(getEntityClass()));
        Query query = entityManager.createQuery(cq);
        if (!all) {
            query.setMaxResults(maxResults);
            query.setFirstResult(firstResult);
        }
        return query.getResultList();
    }

    @Override
    public T read(K key) {
        T entity = entityManager.find(getEntityClass(), key);
        return entity;
    }

    @Override
    public void update(T entity) {
        entityManager.merge(entity);
    }

    @Override
    public void delete(K key) {
        T entity = read(key);
        entityManager.remove(entity);
    }

}
