/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.util;

import mx.neocs.addressbook.config.ApplicationConstants;

import org.jboss.logging.Logger;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class JspUtil {

    private static final Logger LOGGER = Logger.getLogger(JspUtil.class.getName());

    private JspUtil() {
    }

    /**
    * Depending on the server variable of application mode returns the body tag.
     * @return the body tag.
     */
    public static String getBodyStartTag() {
        String applicationMode = ApplicationConstants.getApplicationMode();

        if (applicationMode == null) {
            LOGGER.warnv("The system property \"{0}\" is no set on {1}.",
                    ApplicationConstants.APP_MODE, "standalone.xml");
            return "<body class=\"hold-transition skin-blue sidebar-mini fixed skin-blue-light\">";
        }
        
        switch (applicationMode) {
            case "debug" :
                return "<body class=\"layout-top-nav fixed skin-blue\">";
            case "production" :
                return "<body class=\"layout-top-nav fixed skin-black-light\">";
            default :
                LOGGER.warnv("The value \"{0}\" set on system property \"{1}\" is no valid.", 
                        applicationMode, ApplicationConstants.APP_MODE);
                return "<body class=\"layout-top-nav fixed skin-blue-light\">";
        }
    }
}
