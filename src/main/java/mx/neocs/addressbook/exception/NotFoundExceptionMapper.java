/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.exception;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    private static final String RESTEASY_ERROR_PATTERN = "^(RESTEASY\\d+: )(.+)$";
    private static final Pattern PATTERN = Pattern.compile(RESTEASY_ERROR_PATTERN);

    @Override
    public Response toResponse(NotFoundException ex) {
        Matcher matcher = PATTERN.matcher(ex.getMessage());
        String message = matcher.matches() ? matcher.group(2) : ex.getMessage();
        Error error = new Error(Response.Status.NOT_FOUND.getStatusCode(), message);
        return Response.status(Response.Status.NOT_FOUND).entity(error)
                .type(MediaType.APPLICATION_JSON).build();
    }

}
