/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.exception;

/**
 * This class help to describe the error for the client.
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Error {

    private int statusCode;
    private String message;

    /**
     * Create a new instance with the status code and a message about the error.
     * @param statusCode the satus code.
     * @param message datils about the error.
     */
    public Error(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    /**
     * Get the value of status.
     *
     * @return the value of status
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Set the value of status.
     *
     * @param statusCode new value of status
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Get the value of message.
     *
     * @return the value of message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set the value of message.
     *
     * @param message new value of message
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
