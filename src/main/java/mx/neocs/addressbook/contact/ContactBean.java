/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.contact;

import mx.neocs.addressbook.persistence.ContactEntity;
import mx.neocs.addressbook.persistence.ContactRepository;
import mx.neocs.addressbook.persistence.EmailEntity;
import mx.neocs.addressbook.persistence.PhoneEntity;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
@Stateless
public class ContactBean {

    @Inject
    private ContactRepository contactRepository;

    /**
     * Allows to save in the data source a new contact.
     * @param contact the data of the contact to save.
     * @return the identifier of the contact.
     * @throws IncompleteContactException when the contact data is incomplete.
     */
    public Long create(final Contact contact) throws IncompleteContactException {
        if ((contact.getPhones() == null || contact.getPhones().isEmpty())
                && (contact.getEmails() == null || contact.getEmails().isEmpty())) {
            throw new IncompleteContactException("The contact has not an email or phone number.");
        }

        ContactEntity contactEntity = dtoToEntity(contact);
        contactEntity = contactRepository.create(contactEntity);

        List<Phone> phones = contact.getPhones();

        if (phones != null && !phones.isEmpty()) {
            for (Phone phone : phones) {
                PhoneEntity phoneEntity = new PhoneEntity();
                phoneEntity.setPhone(phone.getPhone());
                phoneEntity.setPhoneType(phone.getPhoneType());
                phoneEntity.setContact(contactEntity);

                contactRepository.add(phoneEntity);
            }
        }

        List<Email> emails = contact.getEmails();

        if (emails != null && !emails.isEmpty()) {
            for (Email email : emails) {
                EmailEntity emailEntity = new EmailEntity();
                emailEntity.setEmail(email.getEmail());
                emailEntity.setEmailType(email.getEmailType());
                emailEntity.setContact(contactEntity);

                contactRepository.add(emailEntity);
            }
        }

        return contactEntity.getId();
    }

    /**
     * Get a specific contact by the identifier.
     * @param contactId the identifier of the contact to look up.
     * @return the contact data.
     */
    public Contact getById(Long contactId) {
        ContactEntity contactEntity = contactRepository.read(contactId);
        Contact contact = entityToDto(contactEntity);
        List<PhoneEntity> phonesEntities = contactRepository.getPhones(contact.getId());

        if (phonesEntities != null && !phonesEntities.isEmpty()) {
            List<Phone> phones = new ArrayList<>();

            phonesEntities.stream().map((phoneEntity) -> {
                Phone phone = new Phone();
                phone.setId(phoneEntity.getId());
                phone.setPhone(phoneEntity.getPhone());
                phone.setPhoneType(phoneEntity.getPhoneType());
                return phone;
            }).forEachOrdered((phone) -> {
                phones.add(phone);
            });

            contact.setPhones(phones);
        }

        List<EmailEntity> emailsEntities = contactRepository.getEmails(contact.getId());

        if (emailsEntities != null && !emailsEntities.isEmpty()) {
            List<Email> emails = new ArrayList<>();

            emailsEntities.stream().map((emailEntity) -> {
                Email email = new Email();
                email.setId(emailEntity.getId());
                email.setEmail(emailEntity.getEmail());
                email.setEmailType(emailEntity.getEmailType());
                return email;
            }).forEachOrdered((email) -> {
                emails.add(email);
            });

            contact.setEmails(emails);
        }

        return contact;
    }

    /**
     * Allows get all the contacts in the data source.
     * @return all the contacts founds in the data source.
     */
    public List<Contact> findAll() {
        List<ContactEntity> entities = contactRepository.findEntities();
        List<Contact> contacts = entitiesToDtos(entities);

        for (Contact contact : contacts) {
            List<PhoneEntity> phonesEntities = contactRepository.getPhones(contact.getId());

            if (phonesEntities != null && !phonesEntities.isEmpty()) {
                List<Phone> phones = new ArrayList<>();

                phonesEntities.stream().map((phoneEntity) -> {
                    Phone phone = new Phone();
                    phone.setId(phoneEntity.getId());
                    phone.setPhone(phoneEntity.getPhone());
                    phone.setPhoneType(phoneEntity.getPhoneType());
                    return phone;
                }).forEachOrdered((phone) -> {
                    phones.add(phone);
                });

                contact.setPhones(phones);
            }

            List<EmailEntity> emailsEntities = contactRepository.getEmails(contact.getId());

            if (emailsEntities != null && !emailsEntities.isEmpty()) {
                List<Email> emails = new ArrayList<>();

                emailsEntities.stream().map((emailEntity) -> {
                    Email email = new Email();
                    email.setId(emailEntity.getId());
                    email.setEmail(emailEntity.getEmail());
                    email.setEmailType(emailEntity.getEmailType());
                    return email;
                }).forEachOrdered((email) -> {
                    emails.add(email);
                });

                contact.setEmails(emails);
            }
        }

        entities = null;
        return contacts;
    }

    /**
     * Allows update a contact.
     * @param contactId the identifier of the contact that will be update.
     * @param contact the contact data.
     * @throws IncompleteContactException when the contact data is incomplete.
     * @throws IncompatibleIdentifierException when the contact identifier in 
     *     the data is different of the contact identifier.
     */
    public void update(Long contactId, Contact contact) throws IncompleteContactException,
            IncompatibleIdentifierException {
        if (!contact.getId().equals(contactId)) {
            throw new IncompatibleIdentifierException("Identifiers are differents");
        }

        ContactEntity contactEntity = contactRepository.read(contactId);
        contactEntity = dtoToEntity(contact, contactEntity);
        contactRepository.update(contactEntity);

        List<Phone> phones = contact.getPhones();

        if (phones != null && !phones.isEmpty()) {
            for (Phone phone : phones) {
                PhoneEntity phoneEntity = contactRepository.findPhone(contactId, phone.getId());
                phoneEntity.setPhone(phone.getPhone());
                phoneEntity.setPhoneType(phone.getPhoneType());

                contactRepository.update(phoneEntity);
            }
        }

        List<Email> emails = contact.getEmails();

        if (emails != null && !emails.isEmpty()) {
            for (Email email : emails) {
                EmailEntity emailEntity = contactRepository.findEmail(contactId, email.getId());
                emailEntity.setEmail(email.getEmail());
                emailEntity.setEmailType(email.getEmailType());

                contactRepository.update(emailEntity);
            }
        }

    }

    /**
     * Allows remove a contact using an identifier.
     * @param contactId the identifier of the contact that will be remove.
     */
    public void delete(Long contactId) {
        List<PhoneEntity> phonesEntities = contactRepository.getPhones(contactId);

        if (phonesEntities != null && !phonesEntities.isEmpty()) {
            phonesEntities.forEach((phoneEntity) -> {
                contactRepository.deletePhone(phoneEntity);
            });
        }

        List<EmailEntity> emailsEntities = contactRepository.getEmails(contactId);

        if (emailsEntities != null && !emailsEntities.isEmpty()) {
            emailsEntities.forEach((emailEntity) -> {
                contactRepository.deleteEmail(emailEntity);
            });
        }

        contactRepository.delete(contactId);
    }

    private ContactEntity dtoToEntity(Contact dto) {
        return dtoToEntity(dto, null);
    }

    private ContactEntity dtoToEntity(Contact dto, ContactEntity entity) {

        if (entity == null) {
            entity = new ContactEntity();
            entity.setId(null);
        } else {
            entity.setId(entity.getId());
        }

        entity.setTitle(dto.getTitle());
        entity.setFirstName(dto.getFirstName());
        entity.setMiddleName(dto.getMiddleName());
        entity.setLastName(dto.getLastName());
        entity.setSuffix(dto.getSuffix());

        return entity;
    }

    private Contact entityToDto(ContactEntity entity) {
        if (entity == null) {
            return null;
        }

        Contact dto = new Contact();
        dto.setId(entity.getId());
        dto.setTitle(entity.getTitle());
        dto.setFirstName(entity.getFirstName());
        dto.setMiddleName(entity.getMiddleName());
        dto.setLastName(entity.getLastName());
        dto.setSuffix(entity.getSuffix());

        return dto;
    }

    private List<Contact> entitiesToDtos(List<ContactEntity> entities) {
        List<Contact> dtos = new ArrayList<>();

        entities.stream().map((entity) -> entityToDto(entity)).forEachOrdered((dto) -> {
            dtos.add(dto);
        });

        return dtos;
    }

}
