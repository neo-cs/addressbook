/*
 * Copyright (C) 2018 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.contact;

import mx.neocs.addressbook.persistence.EmailType;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Email implements Serializable {

    private static final long serialVersionUID = 5431169333070942719L;
    private Long id;
    private String email;
    private EmailType emailType;

    /**
     * Get the value of id.
     *
     * @return the value of id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the value of id.
     *
     * @param id new value of id.
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Get the value of email.
     *
     * @return the value of email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the value of email.
     *
     * @param email new value of email.
     */
    public void setEmail(String email) {
        this.email = email;
    }


    /**
     * Get the value of emailType.
     *
     * @return the value of emailType.
     */
    public EmailType getEmailType() {
        return emailType;
    }

    /**
     * Set the value of emailType.
     *
     * @param emailType new value of emailType.
     */
    public void setEmailType(EmailType emailType) {
        this.emailType = emailType;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.email);
        hash = 59 * hash + Objects.hashCode(this.emailType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Email other = (Email) obj;
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return this.emailType == other.emailType;
    }

    @Override
    public String toString() {
        return "Email {"
                + "email : " + email
                + ", emailType : " + emailType
                + '}';
    }

}
