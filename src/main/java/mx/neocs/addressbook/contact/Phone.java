/*
 * Copyright (C) 2018 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.contact;

import mx.neocs.addressbook.persistence.PhoneType;

import java.io.Serializable;
import java.util.Objects;


/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Phone implements Serializable {

    private static final long serialVersionUID = -2561110092164120828L;
    private Long id;
    private String phone;
    private PhoneType phoneType;

    /**
     * Get the value of id.
     *
     * @return the value of id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the value of id.
     *
     * @param id new value of id.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get the value of phone.
     *
     * @return the value of phone.
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Set the value of phone.
     *
     * @param phone new value of phone.
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Get the value of phoneType.
     *
     * @return the value of phoneType.
     */
    public PhoneType getPhoneType() {
        return phoneType;
    }

    /**
     * Set the value of phoneType.
     *
     * @param phoneType new value of phoneType.
     */
    public void setPhoneType(PhoneType phoneType) {
        this.phoneType = phoneType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.phone);
        hash = 13 * hash + Objects.hashCode(this.phoneType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Phone other = (Phone) obj;
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        return this.phoneType == other.phoneType;
    }

    @Override
    public String toString() {
        return "Phone {" 
                + "phone : " + phone
                + ", phoneType : " + phoneType
                + '}';
    }

}
