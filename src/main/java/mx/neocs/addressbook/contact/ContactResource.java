/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.contact;

import org.jboss.logging.Logger;

import java.net.URI;
import java.text.MessageFormat;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service for manage the contact.
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
@Path("contacts")
@RequestScoped
public class ContactResource {

    @Context
    private UriInfo context;
    @EJB
    private ContactBean contactBean;
    private static final Logger LOGGER = Logger.getLogger(ContactResource.class.getName());
    private static final String MESSAGE_TEMPLATE = "Contact with ID: {0} has been successfully {1}";

    /**
     * Allows the create a new contact.
     * @param contact all the information to create the contact.
     * @return a response with the identifier for the contact.
     * @throws IncompleteContactException if the required information is not present.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(Contact contact) throws IncompleteContactException {
        LOGGER.debugv("Creando: {0}", contact);
        Long idContact = contactBean.create(contact);
        JsonObject idContactJson = Json.createObjectBuilder()
                .add("idContact", idContact)
                .build();
        URI resourcePath = URI.create(context.getAbsolutePath().toString() + '/' + idContact);
        return Response.created(resourcePath).entity(idContactJson).type(MediaType.APPLICATION_JSON)
                .tag("Contact succesful created").build();
    }

    /**
     * Allows to get all the information of the one contact.
     * @param contactId the contact identifier.
     * @return response with a contact information.
     */
    @Path("{contactId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContactById(@PathParam("contactId") Long contactId) {
        LOGGER.debugv("Buscando contacto con ID: {0}", contactId);
        Contact contact = contactBean.getById(contactId);
        return Response.ok(contact).build();
    }
    
    /**
     * Allows to get all the contacts.
     * @return a list with all the contacts.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findContatcs() {
        List<Contact> contacts = contactBean.findAll();
        return Response.ok(contacts).build();
    }

    /**
     * The responsibility of this method is update the contact information.
     * @param contactId the contact identifier that will be update.
     * @param contact all the information that will be change.
     * @return if the contact is updated the return a message like "Contact with ID: {0} has been
     *     successfully {1}".
     * @throws IncompleteContactException if the required information is not present.
     * @throws IncompatibleIdentifierException if the contact identifier in the request is different
     *     in the DTO.
     */    
    @Path("{contactId}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateContact(@PathParam("contactId") Long contactId,
            Contact contact) throws IncompleteContactException, IncompatibleIdentifierException {
        contactBean.update(contactId, contact);
        String message = MessageFormat.format(MESSAGE_TEMPLATE, contactId, "updated");
        JsonObject messageJson = Json.createObjectBuilder()
                .add("message", message).build();

        return Response.ok().entity(messageJson).build();
    }

    /**
     * Allows delete a contact by identifier.
     * @param contactId the contact identifier to delete.
     * @return the response about the delete.
     */
    @Path("{contactId}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteContact(@PathParam("contactId") Long contactId) {
        contactBean.delete(contactId);
        String message = MessageFormat.format(MESSAGE_TEMPLATE, contactId, "deleted");
        JsonObject messageJson = Json.createObjectBuilder()
                .add("message", message).build();

        return Response.ok().entity(messageJson).build();
    }

}
