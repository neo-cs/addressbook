/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.addressbook.config;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class ApplicationConstants {
    
    public static final String APP_MODE = "mx.neocs.addressbook.mode";
    public static final String PERSISTENCE_UNIT = "primary";
    public static final String JTA_DATA_SOURCE = "java:jboss/datasources/adminlteDS";

    private ApplicationConstants() {
    }
    
    
    public static String getApplicationMode() {
        return System.getProperty(APP_MODE);
    }
}
