/* 
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(document).ready(function() {
    $('#contactForm').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            AddressBook.showAlert('danger', 'Can not saved contact!', 'Required fields has errors.');
        } else {
            $("#btnSave").click(e);
        }
    });

    var contactId = AddressBook.readFromWebBrowser('AddressBook.contact.id');
    var contactPhoneId;
    var contactEmailId;

    AddressBook.contacts.getContact(contactId, function (contact) {
        $('#txtTitle').val(contact.title);
        $('#txtFirstName').val(contact.firstName);
        $('#txtMiddleName').val(contact.middleName);
        $('#txtLastName').val(contact.lastName),
        $('#txtSuffix').val(contact.suffix);
        
        if (contact.phones !== 'undefined' && contact.phones !== null && contact.phones.length !== 0) {
            contactPhoneId = contact.phones[0].id;
            $('#txtPhone').val(contact.phones[0].phone);
            $("#slcPhoneType option[value='{0}']".format(contact.phones[0].phoneType)).prop('selected', true);
        } else {
            $('#txtPhone').val('');
            $("#slcPhoneType option[value='MAIN']").prop('selected', true);
        }
        
        if (contact.emails !== 'undefined' && contact.emails !== null && contact.emails.length !== 0) {
            contactEmailId = contact.emails[0].id;
            $('#txtEmail').val(contact.emails[0].email);
            $("#slcEmailType option[value='{0}']".format(contact.emails[0].emailType)).prop('selected', true);
        } else {
            $('#txtEmail').val('');
            $("#slcEmailType option[value='HOME']").prop('selected', true);
        }
    });
    
    // Add the event click to save button.
    $('#btnSave').click(function(e) {
        e.preventDefault();

        var emails;
        var phones;
        
        if (!$('#txtEmail').val().isEmpty()) {
            emails = [{id : contactEmailId, email : $('#txtEmail').val(), emailType : $('#slcEmailType').val()}];
        } else {
            emails = null;
        }
        
        if (!$('#txtPhone').val().isEmpty()) {
            phones = [{id : contactPhoneId, phone : $('#txtPhone').val(), phoneType : $('#slcPhoneType').val()}];
        } else {
            phones = null;
        }
        
        if (emails === null && phones === null) {
            AddressBook.showAlert('danger', 'Can not saved contact!', 'Set an email or a phone number.');
            return;
        }
        
        var contact = {
            id : contactId,
            title : $('#txtTitle').val(),
            firstName : $('#txtFirstName').val(),
            middleName : $('#txtMiddleName').val(),
            lastName : $('#txtLastName').val(),
            suffix : $('#txtSuffix').val(),
            phones : phones,
            emails : emails
        };

        AddressBook.contacts.updateContact(contact, function (data, textStatus, jqXHR) {
            AddressBook.showAlert('success', 'Contact updated!', 'Contact was updated successfully.');
        }, function (jqXHR, textStatus, errorThrown) {
            AddressBook.showAlert('danger', 'Can not updated contact!', 'Were a problem while the contact was going to update.');
        });
    });

    // Add the event click to cancel button.
    $('#btnCancel').click(function () {
        AddressBook.deleteFromWebBrowser('AddressBook.contact.id');
        window.location = AddressBook.getContextPath();
    });
});