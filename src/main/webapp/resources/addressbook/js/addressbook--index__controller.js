/* 
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(document).ready(function () {
    $('.nav > li:nth-child(1)').addClass("active");
    $('.nav > li:nth-child(1) > a').append('<span class="sr-only">(current)</span>');

    AddressBook.contacts.getContacts(function (contacts) {
        var tableBody = $('#tblContacts > tbody');
        var row;

        tableBody.find('tr').remove();

        if (contacts.length === 0) {
            row = '<tr><td class="text-center" colspan="2">Address book has not contacts.</td></tr>';
            $(tableBody).append(row);
        } else {
            for (let i = 0; i < contacts.length; i++) {
                let contact = contacts[i];
                let rowTemplate
                        = '<tr>'
                        + '<td class="hidden id">{0}</td>'
                        + '<td>{1} {2} {3} {4} {5}</td>'
                        + '<td>{6} <div class="pull-right hidden contact-options"><span class="contact-edit" role="button" data-tooltip="Edit contact"><i class="glyphicon glyphicon-pencil"></i> <span class="sr-only">Edit contact</span></span> <span class="contact-delete" role="button" data-tooltip="Delete contact"><i class="glyphicon glyphicon-trash"></i> <span class="sr-only">Delete contact</span></span></div></td>'
                        + '</tr>';
                row = rowTemplate.format(contact.id, contact.title, contact.firstName,
                        contact.middleName, contact.lastName, contact.suffix,
                        contact.phones !== 'undefined' && contact.phones !== null && contact.phones.length !== 0 ? contact.phones[0].phone : '');
                $(tableBody).append(row);
            }
            
            let contactsRows = $('#tblContacts > tbody > tr');
            
            contactsRows.mouseenter(function (){
                $(this).find('div.contact-options').toggleClass('hidden');
            });

            contactsRows.mouseleave(function (){
                $(this).find('div.contact-options').toggleClass('hidden');
            });
            
            contactsRows.click(function () {
                let contactId = parseInt($(this).children("td.id").text());
                let contactModal = $('#contactModal');
                
                AddressBook.contacts.getContact(contactId, function (contact) {
                    let contactHeaderTemplate = '<i class="glyphicon glyphicon-user"></i> {0} {1} {2} {3} {4}';
                    let contactBodyTemplate
                            = '<div class="form-horizontal">'
                            + '<div class="form-group"> <label class="control-label col-xs-2"><i class="glyphicon glyphicon-earphone"></i> <span class="sr-only">Phone</span></label> <div class="col-xs-10"> <p class="form-control-static">{0} <span class="small text-muted">{1}</span></p></div></div>'
                            + '<div class="form-group"> <label class="control-label col-xs-2"><i class="glyphicon glyphicon-envelope"></i> <span class="sr-only">Email</span></label> <div class="col-xs-10"> <p class="form-control-static">{2} <span class="small text-muted">{3}</span></p></div></div>'
                            +'</div>';
                    
                    contactModal.find('.modal-title').empty().append(
                            contactHeaderTemplate.format(contact.title,
                                contact.firstName, contact.middleName,
                                contact.lastName, contact.suffix));

                    contactModal.find('.modal-body').empty()
                            .append(contactBodyTemplate.format(
                                contact.phones !== 'undefined' && contact.phones !== null && contact.phones.length !== 0 ? contact.phones[0].phone : '',                        
                                contact.phones !== 'undefined' && contact.phones !== null && contact.phones.length !== 0 ? contact.phones[0].phoneType : '',                        
                                contact.emails !== 'undefined' && contact.emails !== null && contact.emails.length !== 0 ? contact.emails[0].email : '',
                                contact.emails !== 'undefined' && contact.emails !== null && contact.emails.length !== 0 ? contact.emails[0].emailType : ''
                            ));
                });

                contactModal.modal('show');
            });

            $('#tblContacts > tbody > tr span.contact-edit').click(function(e) {
                e.stopImmediatePropagation();
                
                let trParent = $(this).parents()[2];
                let contactId = parseInt($(trParent).find('td.id').text());
                AddressBook.saveInWebBrowser('AddressBook.contact.id', contactId);
                window.location = AddressBook.getContextPath() + '/contacts/edit-contact.xhtml';
            });
            
            $('#tblContacts > tbody > tr span.contact-delete').click(function(e) {
                e.stopImmediatePropagation();
                let trParent = $(this).parents()[2];
                let contactId = parseInt($(trParent).find('td.id').text());
                let contactRemoveModal = $('#contactRemoveModal');

                AddressBook.contacts.getContact(contactId, function (contact) {
                    let contactHeaderTemplate = '<i class="glyphicon glyphicon-user"></i> {0} {1} {2} {3} {4}';
                    let contactBodyTemplate
                            = '<div class="form-horizontal">'
                            + '<div class="form-group"> <label class="control-label col-xs-2"><i class="glyphicon glyphicon-earphone"></i> <span class="sr-only">Phone</span></label> <div class="col-xs-10"> <p class="form-control-static">{0} <span class="small text-muted">{1}</span></p></div></div>'
                            + '<div class="form-group"> <label class="control-label col-xs-2"><i class="glyphicon glyphicon-envelope"></i> <span class="sr-only">Email</span></label> <div class="col-xs-10"> <p class="form-control-static">{2} <span class="small text-muted">{3}</span></p></div></div>'
                            +'</div>';
                    
                    contactRemoveModal.find('.modal-title').empty().append(
                            contactHeaderTemplate.format(contact.title,
                                contact.firstName, contact.middleName,
                                contact.lastName, contact.suffix));

                    contactRemoveModal.find('.modal-body').empty()
                            .append(contactBodyTemplate.format(
                                contact.phones !== 'undefined' && contact.phones !== null && contact.phones.length !== 0 ? contact.phones[0].phone : '',                        
                                contact.phones !== 'undefined' && contact.phones !== null && contact.phones.length !== 0 ? contact.phones[0].phoneType : '',                        
                                contact.emails !== 'undefined' && contact.emails !== null && contact.emails.length !== 0 ? contact.emails[0].email : '',
                                contact.emails !== 'undefined' && contact.emails !== null && contact.emails.length !== 0 ? contact.emails[0].emailType : ''
                            ));

                    contactRemoveModal.find('.confirm-delete').unbind('click').click(function (e) {
                        contactRemoveModal.modal('hide');

                        AddressBook.contacts.deleteContact(contactId, function (data, textStatus, jqXHR) {
                            AddressBook.showAlert('success', 'Contact deleted!', 'Contact was delted successfully.');
                            trParent.remove();
                        }, function () {
                            AddressBook.showAlert('danger', 'Can not deleted contact!', 'Were a problem while the contact was going to delete.');
                        });
                    });
                });
                
                contactRemoveModal.modal('show');
            });
        }
    });
});

