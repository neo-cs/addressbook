/* 
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(document).ready(function(){
    // Add click event to the Log In button.
    $("#btnLogin").click(function() {
        var credential = {
            username : $('#txtUsername').val(),
            password : $('#txtPassword').val()
        };
        login(credential);
    });
    
    $('#txtPassword').keypress(function (event) {
        if (event.keyCode === 13) {
           $("#btnLogin").trigger('click');
        }
    });

    /**
     * 
     * @param {object} credential
     */
    function login(credential) {
        var path = AddressBook.getContextPath() + '/login.do';
        var index = AddressBook.getContextPath();

        $.ajax({
            method : 'POST',
            url : path,
            data : credential,
            dataType : 'json',
            success: function(data, textStatus, jqXHR) {
                console.log('Estado: ' + textStatus + ' ' + jqXHR.status);
                console.log('Datos: ' + data);
                if(jqXHR.status === 200){
                    window.location = index;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400) {
                    console.log('Código: 400');
                    console.log('Estado: ' + textStatus);
                    console.log('Error: ' + errorThrown);
                } else if(jqXHR.status === 500) {
                    console.log('Código: 500');
                    console.log('Estado: ' + textStatus);
                    console.log('Error: ' + errorThrown);
                } else if(jqXHR.status === 403){
                    console.log('Código: 403');
                    console.log('Estado: ' + textStatus);
                    console.log('Error: ' + errorThrown);
                } else {
                    console.log('Código: ' + jqXHR.status);
                    console.log('Estado: ' + textStatus);
                    console.log('Error: ' + errorThrown);
                }
            }
        });
    }
});
