/* 
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(document).ready(function() {
    $('.nav > li:nth-child(2)').addClass("active");
    $('.nav > li:nth-child(2) > a').append('<span class="sr-only">(current)</span>');
    
    $('#contactForm').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            AddressBook.showAlert('danger', 'Can not saved contact!', 'Required fields has errors.');
        } else {
            $("#btnSave").click(e);
        }
    });
    
    // Add the event click to save button.
    $("#btnSave").click(function(e) {
        e.preventDefault();

        var emails;
        var phones;
        
        if (!$("#txtEmail").val().isEmpty()) {
            emails = [{email : $("#txtEmail").val(), emailType : $('#slcEmailType').val()}];
        } else {
            emails = null;
        }
        
        if (!$("#txtPhone").val().isEmpty()) {
            phones = [{phone : $("#txtPhone").val(), phoneType : $('#slcPhoneType').val()}];
        } else {
            phones = null;
        }
        
        if (emails === null && phones === null) {
            AddressBook.showAlert('danger', 'Can not saved contact!', 'Set an email or a phone number.');
            return;
        }
        
        var contact = {
            id : null,
            title : $("#txtTitle").val(),
            firstName : $("#txtFirstName").val(),
            middleName : $("#txtMiddleName").val(),
            lastName : $("#txtLastName").val(),
            suffix : $("#txtSuffix").val(),
            phones : phones,
            emails : emails
        };

        AddressBook.contacts.addContact(contact, function (data, textStatus, jqXHR) {
            AddressBook.showAlert('success', 'Contact saved!', 'Contact was saved successfully.');
            $("#txtTitle").val('');
            $("#txtFirstName").val('');
            $("#txtMiddleName").val('');
            $("#txtLastName").val(''),
            $("#txtSuffix").val('');
            $("#txtPhone").val('');
            $("#slcPhoneType option[value='MAIN']").prop('selected', true);
            $("#txtEmail").val('');
            $("#slcEmailType option[value='HOME']").prop('selected', true);
        }, function (jqXHR, textStatus, errorThrown) {
            AddressBook.showAlert('danger', 'Can not saved contact!', 'Were a problem while the contact was going to save.');
        });
    });
    
    // Add the event click to cancel button.
    $('#btnCancel').click(function () {
        window.location = AddressBook.getContextPath();
    });
});
