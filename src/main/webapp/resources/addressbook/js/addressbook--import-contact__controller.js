/* 
 * Copyright (C) 2018 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(document).ready(function () {
    var dragArea = document.getElementById('output')
    dragArea.textContent += 'Drop files here from file browser.'
    dragArea.addEventListener('dragenter', handleDragEnter, false)
    dragArea.addEventListener('dragover', handleDragOver, false)
    dragArea.addEventListener('drop', handleDrop, false)

    function handleDragEnter (event) {
        document.getElementById('output').textContent = ''
        event.stopPropagation()
        event.preventDefault()
    }

    function handleDragOver (event) {
        event.stopPropagation()
        event.preventDefault()
    }

    function handleDrop (event) {
        var dt = event.dataTransfer
        var files = dt.files
        var count = files.length

        event.stopPropagation()
        event.preventDefault()
        output("File Count: " + count + "\n")

        for (var i = 0; i < files.length; i++) {
            output(" File " + i + ":\n(" + (typeof files[i]) + ") : <" + files[i] + " > " +
            files[i].name + " " + files[i].size + "\n")
        }
    }

    function output (text) {
        document.getElementById("output").textContent += text
    }
});
