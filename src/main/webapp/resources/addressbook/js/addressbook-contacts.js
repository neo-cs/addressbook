/* 
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// This define the contacts submodule.
var AddressBook = (function (app) {
    var contacts = {};
    
    /**
     * Save a contact.
     * @param {Object} contact the contact that will be save.
     * @param {Function} work a callback funtion.
     */
    contacts.addContact = function(contact, work, fail) {
        
        $.ajax({
            method: 'POST',
            url: app.getContextPath() + '/rs/contacts',
            data: JSON.stringify(contact),
            dataType: "json",
            contentType: 'application/json',
            success: function (data, textStatus, jqXHR) { 
                work(data, textStatus, jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error status: " + textStatus);
                console.log("Error: " + errorThrown);
                fail();
            }
        });
    };
    
    /**
     * Get a contact using the identifier.
     * @param {Number} contactId the contact identifier.
     * @param {Function} work a callback funtion.
     * @returns {Object} a contact.
     */
    contacts.getContact = function(contactId, work) {
        $.get(app.getContextPath() + '/rs/contacts/' + contactId, function (data) {
            work(data);
        });
    };
    
    /**
     * Get all the contacts.
     * @param {Function} work a callback funtion.
     * @returns {Array} an array of contacts.
     */
    contacts.getContacts = function(work) {
        $.get(app.getContextPath() + '/rs/contacts', function (data) {
            work(data);
        });
    };

    contacts.updateContact = function (contact, work, fail) {
        $.ajax({
            method: "PUT",
            url: app.getContextPath() + '/rs/contacts/' + contact.id,
            data: JSON.stringify(contact),
            dataType: "json",
            contentType: 'application/json',
            success: function (data, textStatus, jqXHR) {
                work(data, textStatus, jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error status: " + textStatus);
                console.log("Error: " + errorThrown);
                fail();
            }
        });
    };
    
    contacts.deleteContact = function (contactId, work, fail) {
        $.ajax({
            method: "DELETE",
            url: app.getContextPath() + '/rs/contacts/' + contactId,
            success: function (data, textStatus, jqXHR) { 
                work(data, textStatus, jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error status: " + textStatus);
                console.log("Error: " + errorThrown);
                fail();
            }
        });
    };

    app.contacts = contacts;
    return app;
}) (AddressBook);