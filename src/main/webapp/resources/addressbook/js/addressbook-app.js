/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// This define the AddressBook module.
var AddressBook = (function () {
    var app = {};
    var contextPath;

    /**
     * Set the context path of application.
     * @param {String} context the application context path.
     */
    app.setContextPath = function (context) {
        contextPath = context;
    };
    
    /**
     * Get context path of application.
     * @returns {context} the application context path
     */
    app.getContextPath = function () {
        return contextPath;
    };
    
    /**
     * Update the theme of application.
     */
    app.updateTheme = function() {
        $.ajax(
            {
                type: 'GET',
                url: contextPath + '/rs/ui/theme',
                dataType: "text",
                success: function (data, textStatus, jQxhr) {
                    $('body').addClass(data);
                },
                error: function (error) {
                    $('body').addClass('skin-blue');
                }
            }
        );
    };
    
    app.readFromWebBrowser = function (key) {
        if (typeof(Storage) !== "undefined") {
            return sessionStorage.getItem(key);
        } else {
            let name = key + "=";
            let decodedCookie = decodeURIComponent(document.cookie);
            let ca = decodedCookie.split(';');

            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];

                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }

                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    };

    app.saveInWebBrowser = function (key, value) {
        if (typeof(Storage) !== "undefined") {
            sessionStorage.setItem(key, value);
        } else {
            var d = new Date();
            d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = key + "=" + value + ";" + expires + ";path=/";
        }
    };

    app.deleteFromWebBrowser = function (key) {
        if (typeof(Storage) !== "undefined") {
            sessionStorage.removeItem(key);
        } else {
            document.cookie = key + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
        }
    };
    
    app.showAlert = function (type, title, message) {
        let alertClassTemplate = 'alert alert-{0} alert-dismissible';
        let alertTemplate = '<div id="{4}" class="{0}"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa {1}"></i> {2}</h4>{3}</div>';
        let alertClass;
        let alertIcon;
        let id = 'alert-{0}'.format(Math.floor(Math.random() * 100000));
        
        switch (type) {
            case 'info':
                alertClass = alertClassTemplate.format('info');
                alertIcon = 'fa-info';
                break;
            case 'success':
                alertClass = alertClassTemplate.format('success');
                alertIcon = 'fa-check';
                break;
            case 'warning':
                alertClass = alertClassTemplate.format('warnig');
                alertIcon = 'fa-warning';
                break;
            case 'danger':
                alertClass = alertClassTemplate.format('danger');
                alertIcon = 'fa-ban';
                break;
        }

        let alert = alertTemplate.format(alertClass, alertIcon, title, message, id);
        $('section.content').prepend(alert);
        setTimeout(function () {$('#' + id).remove(); }, 4 * 1000);
    };

    return app;
})();

$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.restart();
    });

    AddressBook.updateTheme();
});
