# Create a new contact
curl -i -X POST -H 'Content-Type: application/json' -d '{ "id": "", "title": "Mr", "firstName" : "Simon", "middleName" : "J.", "lastName" : "Parson", "suffix" : "III"}' http://localhost:8080/addressbook/rs/contacs

# Read a contact with ID 2
curl -i -X GET http://localhost:8080/addressbook/rs/contacs/2

# Read a list of contact like a same name
curl -i -X GET http://localhost:8080/addressbook/rs/contacs?name=John

# Update a contact
curl -i -X PUT -H 'Content-Type: application/json' -d '{"id": "2", "title": "Mr", "firstName" : "Simon", "middleName" : "Jonathan", "lastName" : "Parson", "suffix" : "III"}' http://localhost:8080/addressbook/rs/contacs

# Delete a contact with ID 2
curl -i -X DELETE http://localhost:8080/addressbook/rs/contacs/3
