/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mx.neocs.addressbook.contact;

import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.json.Json;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Path;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import mx.neocs.addressbook.config.ApplicationConstants;
import mx.neocs.addressbook.config.RestfulConfiguration;
import mx.neocs.addressbook.persistence.ContactEntity;
import mx.neocs.addressbook.persistence.ContactRepository;
import mx.neocs.addressbook.persistence.GenericRepository;
import mx.neocs.addressbook.persistence.Repository;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import mx.neocs.addressbook.exception.ValidationException;
import mx.neocs.addressbook.persistence.EmailEntity;
import mx.neocs.addressbook.persistence.EmailType;
import mx.neocs.addressbook.persistence.PhoneEntity;
import mx.neocs.addressbook.persistence.PhoneType;

import static org.junit.Assert.assertTrue;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
@RunWith(Arquillian.class)
public class ContactResourceTest {

    private static final String RESOURCE_PREFIX = RestfulConfiguration.class.getAnnotation(ApplicationPath.class).value().substring(1);
    private static final String RESOURCE_NAME = ContactResource.class.getAnnotation(Path.class).value();
    private static final Logger LOG = Logger.getLogger(ContactResourceTest.class.getName());
    
    @Deployment(testable = false)
    public static WebArchive createDeployment() {
        WebArchive war = ShrinkWrap.create(WebArchive.class, "contact-test.war");
        war.addAsWebInfResource("WEB-INF/test-beans.xml", "beans.xml");
        war.addAsWebInfResource("WEB-INF/test-addressbook-ds.xml", "addressbook-ds.xml");
        war.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml");
        war.addAsManifestResource("log4j-jboss.properties", "log4j.properties");
        war.addClass(ApplicationConstants.class);
        war.addClass(Contact.class);
        war.addClass(ContactBean.class);
        war.addClass(ContactResource.class);
        war.addClass(ContactEntity.class);
        war.addClass(ContactRepository.class);
        war.addClass(Email.class);
        war.addClass(EmailEntity.class);
        war.addClass(EmailType.class);
        war.addClass(GenericRepository.class);
        war.addClass(IncompatibleIdentifierException.class);
        war.addClass(IncompleteContactException.class);
        war.addClass(Phone.class);
        war.addClass(PhoneEntity.class);
        war.addClass(PhoneType.class);
        war.addClass(Repository.class);
        war.addClass(RestfulConfiguration.class);
        war.addClass(ValidationException.class);

        return war;
    }
    
    @Test
    @RunAsClient
    public void createTest(@ArquillianResource URL deploymentUrl) throws InterruptedException {
        String uri = deploymentUrl.toString() + RESOURCE_PREFIX;
        
        List<ContactResourceThread> list = new ArrayList<>();
        
        for(int i = 0; i <= 1000; i++) {
            ContactResourceThread contactResourceThread = new ContactResourceThread("Cliente " + i, uri);
            list.add(contactResourceThread);
        }

        assertConcurrent("Prueba de concurrencia", list, 2 * 60);
    }
    
    
    public static void assertConcurrent(final String message, final List<? extends Runnable> runnables, final int maxTimeoutSeconds) throws InterruptedException {
        final int numThreads = runnables.size();
        final List<Throwable> exceptions = Collections.synchronizedList(new ArrayList<Throwable>());
        final ExecutorService threadPool = Executors.newFixedThreadPool(numThreads);
        try {
            final CountDownLatch allExecutorThreadsReady = new CountDownLatch(numThreads);
            final CountDownLatch afterInitBlocker = new CountDownLatch(1);
            final CountDownLatch allDone = new CountDownLatch(numThreads);
            for (final Runnable submittedTestRunnable : runnables) {
                threadPool.submit(new Runnable() {
                    @Override
                    public void run() {
                        allExecutorThreadsReady.countDown();
                        try {
                            afterInitBlocker.await();
                            submittedTestRunnable.run();
                        } catch (final InterruptedException e) {
                            exceptions.add(e);
                        } finally {
                            allDone.countDown();
                        }
                    }
                });
            }
            // wait until all threads are ready
            assertTrue("Timeout initializing threads! Perform long lasting initializations before passing runnables to assertConcurrent", allExecutorThreadsReady.await(runnables.size() * 10, TimeUnit.MILLISECONDS));
            // start all test runners
            afterInitBlocker.countDown();
            assertTrue(message + " timeout! More than" + maxTimeoutSeconds + "seconds", allDone.await(maxTimeoutSeconds, TimeUnit.SECONDS));
        }
        finally {
            threadPool.shutdownNow();
        }
        assertTrue(message + "failed with exception(s)" + exceptions, exceptions.isEmpty());
    }

    class ContactResourceThread extends Thread {
        private final String uri;

        public ContactResourceThread(String name, String uri) {
            super(name);
            this.uri = uri;
        }

        @Override
        public void run() {
            Client client = ClientBuilder.newClient();
            LOG.infov("{1} URL: {0}", uri, getName());
            WebTarget webTarget = client.target(uri).path(RESOURCE_NAME);
            Contact requestEntity = new Contact();
            requestEntity.setId(null);
            requestEntity.setTitle("Mr");
            requestEntity.setFirstName("John");
            requestEntity.setMiddleName("J.");
            requestEntity.setLastName("Wick");
            requestEntity.setSuffix("III");
            Response post = webTarget.request(APPLICATION_JSON).post(Entity.json(requestEntity));
            String entity = post.readEntity(String.class);
            LOG.infov("{1} Status: {0}", post.getStatus(), getName());
            LOG.infov("{1} Entity: {0}", entity, getName());
            LOG.infov("{1} Entity Tag: {0}", post.getEntityTag(), getName());

            try (StringReader stringReader = new StringReader(entity)) {
                try (JsonReader jsonReader = Json.createReader(stringReader)) {
                    JsonObject jsonObject = jsonReader.readObject();
                    JsonNumber idContactJsonNumber = jsonObject.getJsonNumber("idContact");
                    long idContact = idContactJsonNumber.longValue();
                    LOG.infov("{1} ID contact: {0}", idContact, getName());
                }
            }
            // assertTrue(Response.Status.CREATED == post.getStatusInfo());
            client.close();
        }
    }
}
