# Address book
This project is a Web application for a simple address book. In this I use Java EE
(JSF, JAX-RS, JPA, Servlet), HTML, CSS, Javascript ([AdminLTE](https://adminlte.io) as web template, 
[jQuery](https://jquery.com), [Bootstrap](https://getbootstrap.com)).

## Installation

### Requirements
This project require:
* [JDK 1.8](http://www.oracle.com/technetwork/java/javase/8-whats-new-2157071.html)
* [Wild<b>Fly</b>](http://wildfly.org) 10.1.0.Final as Java EE application server.
* [H2](http://www.h2database.com) (embedded in Wild<b>Fly</b>) as database engine.
* [Maven](https://maven.apache.org)

### Building from sources
This project is ready for compile using Maven.
- Compile with `mvn clean install`
- For to run, start your application server and deploy the war file that maven
create on target folder.

## License
Please refer to file [LICENSE](LICENSE.md), available in this repository.
